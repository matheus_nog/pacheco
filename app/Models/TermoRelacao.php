<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermoRelacao extends Model
{
    use HasFactory;
    protected $table = 'wp_term_relationships';
    public $timestamps = false;
}
