<?php

use App\Models\Imovel;
use App\Models\Info;
use App\Models\Taxonomy;
use App\Models\Termo;
use App\Models\TermoRelacao;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return Imovel::query()->where('ID', '79662')->first();

    return view('welcome');
});

Route::post('/importar', function () {

    $url = public_path('/assets/xml/Teste.xml');
    $data = file_get_contents($url);
    $data = str_replace("iso-8859-1", "utf-8", $data);
    $xml = simplexml_load_string($data);
    $json = json_encode($xml, JSON_UNESCAPED_UNICODE);
    $array = json_decode($json, TRUE);

    foreach ($array['Imoveis']["Imovel"] as $imovel) {
        set_time_limit(0);

        $exist = Info::query()->where('meta_value', $imovel['CodigoImovel'])->get()->first();
        if (!$exist) {
            $post_title = $imovel["Endereco"];
            $post_title = mb_convert_encoding($post_title, 'UTF-8', 'auto');
            $post_name = str_replace("  ", "-", strtolower($imovel["CodigoImovel"]));
            $post_name = str_replace(" ", "-", $post_name);
            $post_content = '<!-- wp:paragraph --><p>' . $imovel["Descricao"] . '</p><!-- /wp:paragraph -->';

            if (isset($imovel["NumFotos"]) && $imovel["NumFotos"] == 1) {
                $fotos = $imovel["Fotos"]["Foto"]["NomeArquivo"];
            } else {
                $teste = [];
                if (isset($imovel["Fotos"]["Foto"]) && $imovel["Fotos"]["Foto"] != null) {
                    foreach ($imovel["Fotos"]["Foto"] as $item) {
                        $teste[] = $item["NomeArquivo"];
                    }
                    $fotos = implode("|", $teste);
                } else {
                    $fotos = '';
                }
            }

            $post = new Imovel();
            $post->post_title = $post_title;
            $post->post_content = $post_content;
            $post->post_name = $post_name;
            $post->post_date = now();
            $post->post_date_gmt = now();
            $post->post_modified = now();
            $post->post_modified_gmt = now();
            $post->post_author = 1;
            $post->post_status = 'publish';
            $post->post_type = 'imoveis';
            $post->post_excerpt = '';
            $post->to_ping = '';
            $post->pinged = '';
            $post->post_content_filtered = '';
            // wp_posts
            $post->save();

            // cidades            
            $cidade = Termo::query()->where('name', $imovel["Cidade"])->latest('term_id')->first();
            if (!$cidade) {
                // wp_terms
                $novaCidade = new Termo();
                $novaCidade->name = $imovel["Cidade"];
                $novaCidade->slug = str_replace(" ", "-", strtolower($imovel["Cidade"]));
                $novaCidade->save();

                // wp_term_taxonomy
                $taxonomy = new Taxonomy();
                $taxonomy->term_taxonomy_id = $novaCidade->id;
                $taxonomy->term_id = $novaCidade->id;
                $taxonomy->description = '';
                $taxonomy->taxonomy = 'cidades';
                $taxonomy->parent = 0;
                $taxonomy->save();

                // wp_term_relationships
                $termoRelacao = new TermoRelacao();
                $termoRelacao->object_id = $post->id;
                $termoRelacao->term_taxonomy_id = $novaCidade->id;
                $termoRelacao->save();

                $cidade = $novaCidade;
            } else {
                // wp_term_relationships           
                $termoRelacao = new TermoRelacao();
                $termoRelacao->object_id = $post->id;
                $termoRelacao->term_taxonomy_id = $cidade->term_id;
                $termoRelacao->save();
            }

            // bairros
            $bairro = Termo::query()->where('name', $imovel["Bairro"])->first();
            if (!$bairro) {
                $novoBairro = new Termo();
                $novoBairro->name = $imovel["Bairro"];
                $novoBairro->slug = str_replace(" ", "-", strtolower($imovel["Bairro"]));
                $novoBairro->save();

                // wp_term_taxonomy
                $taxonomy = new Taxonomy();
                $taxonomy->term_taxonomy_id = $novoBairro->id;
                $taxonomy->term_id = $novoBairro->id;
                $taxonomy->description = '';
                $taxonomy->taxonomy = 'cidades';
                $taxonomy->parent = $cidade->id ? $cidade->id : $cidade->term_id;
                $taxonomy->save();

                // wp_term_relationships
                $termoRelacao = new TermoRelacao();
                $termoRelacao->object_id = $post->id;
                $termoRelacao->term_taxonomy_id = $novoBairro->id;
                $termoRelacao->save();
            } else {
                // wp_term_relationships
                $termoRelacao = new TermoRelacao();
                $termoRelacao->object_id = $post->id;
                $termoRelacao->term_taxonomy_id = $bairro->term_id;
                $termoRelacao->save();

                // wp_term_taxonomy
                $bairros = Termo::query()->where('name', $imovel['Bairro'])->get();

                $taxonomy = null;
                foreach ($bairros as $bairro) {
                    $taxonomy = Taxonomy::query()->where('term_id', $bairro->term_id)->where('parent', $cidade->term_id ? $cidade->term_id : $cidade->id)->first();
                    if ($taxonomy) {
                        break;
                    }
                }

                // $taxonomy = Taxonomy::query()->where('term_id', $bairro->term_id)->where('parent', $cidade->term_id)->first();
                // dd($taxonomy, $bairro->term_id, $cidade->term_id);
                if (!$taxonomy) {
                    // criar novo bairro
                    $novoBairro = new Termo();
                    $novoBairro->name = $imovel["Bairro"];
                    $novoBairro->slug = str_replace(" ", "-", strtolower($imovel["Bairro"]));
                    $novoBairro->save();

                    // wp_term_taxonomy
                    $taxo = new Taxonomy();
                    $taxo->term_taxonomy_id = $novoBairro->id;
                    $taxo->term_id = $novoBairro->id;
                    $taxo->description = '';
                    $taxo->taxonomy = 'cidades';
                    $taxo->parent = $cidade->term_id ? $cidade->term_id : $cidade->id;
                    $taxo->save();
                }
            }

            // tipo imovel
            $tipoImovel = Termo::query()->where('name', $imovel["TipoImovel"])->first();
            // wp_term_relationships
            $termoRelacao = new TermoRelacao();
            $termoRelacao->object_id = $post->id;
            $termoRelacao->term_taxonomy_id = $tipoImovel->term_id;
            $termoRelacao->save();

            //subtipo imovel
            $subTipo = Termo::query()->where('name', $imovel["SubTipoImovel"])->latest('term_id')->first();
            // wp_term_relationships
            $subRelacao = new TermoRelacao();
            $subRelacao->object_id = $post->id;
            $subRelacao->term_taxonomy_id = $subTipo->term_id;
            $subRelacao->save();

            $sql =  "UPDATE wp_term_taxonomy tt
                            SET count =
                            (SELECT count(p.ID) FROM  wp_term_relationships tr
                            LEFT JOIN wp_posts p
                            ON (p.ID = tr.object_id AND p.post_type = 'imoveis' AND p.post_status = 'publish')
                            WHERE tr.term_taxonomy_id = tt.term_taxonomy_id
                        )";
            DB::statement($sql);

            $servico = null;
            switch ($imovel["Servico"]) {
                case 'L':
                    $servico = 'Alugar';
                    break;
                case 'V':
                    $servico = 'Vender';
                    break;
            }

            try {
                $data = [
                    ['post_id' => $post->id, 'meta_key' => 'status', 'meta_value' => $servico],
                    ['post_id' => $post->id, 'meta_key' => 'referencia', 'meta_value' => $imovel["CodigoImovel"]],
                    ['post_id' => $post->id, 'meta_key' => 'cep', 'meta_value' => $imovel["CEP"]],
                    ['post_id' => $post->id, 'meta_key' => 'valor_de_venda', 'meta_value' => $imovel["PrecoVenda"]],
                    ['post_id' => $post->id, 'meta_key' => 'aluguel', 'meta_value' => $imovel["PrecoLocacao"]],
                    ['post_id' => $post->id, 'meta_key' => 'condominio', 'meta_value' => $imovel["PrecoCondominio"]],
                    ['post_id' => $post->id, 'meta_key' => 'iptu', 'meta_value' => $imovel["IPTU"]],
                    ['post_id' => $post->id, 'meta_key' => 'area_util', 'meta_value' => $imovel["AreaUtil"]],
                    ['post_id' => $post->id, 'meta_key' => 'metragem', 'meta_value' => $imovel["AreaTotal"]],
                    ['post_id' => $post->id, 'meta_key' => 'content', 'meta_value' => $imovel["Descricao"]],
                    ['post_id' => $post->id, 'meta_key' => 'infraestrutura', 'meta_value' => $imovel["Infraestrutura"]],
                    ['post_id' => $post->id, 'meta_key' => 'dormitorios', 'meta_value' => $imovel["QtdDormitorios"]],
                    ['post_id' => $post->id, 'meta_key' => 'suites', 'meta_value' => $imovel["QtdSuites"]],
                    ['post_id' => $post->id, 'meta_key' => 'banheiros', 'meta_value' => $imovel["QtdBanheiros"]],
                    ['post_id' => $post->id, 'meta_key' => 'salas', 'meta_value' => $imovel["QtdSalas"]],
                    ['post_id' => $post->id, 'meta_key' => 'vagas', 'meta_value' => $imovel["QtdVagas"]],
                    ['post_id' => $post->id, 'meta_key' => 'elevadores', 'meta_value' => $imovel["QtdElevador"]],
                    ['post_id' => $post->id, 'meta_key' => 'unidades_por_andar', 'meta_value' => $imovel["QtdUnidadesAndar"]],
                    ['post_id' => $post->id, 'meta_key' => 'andares', 'meta_value' => $imovel["QtdAndar"]],
                    ['post_id' => $post->id, 'meta_key' => 'ano_de_construcao', 'meta_value' => $imovel["AnoConstrucao"]],
                    ['post_id' => $post->id, 'meta_key' => 'armario_de_cozinha', 'meta_value' => $imovel["ArmarioCozinha"]],
                    ['post_id' => $post->id, 'meta_key' => 'armario_embutido', 'meta_value' => $imovel["ArmarioEmbutido"]],
                    ['post_id' => $post->id, 'meta_key' => 'churrasqueira', 'meta_value' => $imovel["Churrasqueira"]],
                    ['post_id' => $post->id, 'meta_key' => 'copa', 'meta_value' => $imovel["Copa"]],
                    ['post_id' => $post->id, 'meta_key' => 'entrada_de_caminhoes', 'meta_value' => $imovel["EntradaCaminhoes"]],
                    ['post_id' => $post->id, 'meta_key' => 'esquina', 'meta_value' => $imovel["Esquina"]],
                    ['post_id' => $post->id, 'meta_key' => 'jardim', 'meta_value' => $imovel["Jardim"]],
                    ['post_id' => $post->id, 'meta_key' => 'piscina', 'meta_value' => $imovel["Piscina"]],
                    ['post_id' => $post->id, 'meta_key' => 'playground', 'meta_value' => $imovel["Playground"]],
                    ['post_id' => $post->id, 'meta_key' => 'quadra_poliesportiva', 'meta_value' => $imovel["QuadraPoliEsportiva"]],
                    ['post_id' => $post->id, 'meta_key' => 'salao_de_festas', 'meta_value' => $imovel["SalaoFestas"]],
                    ['post_id' => $post->id, 'meta_key' => 'salao_de_jogos', 'meta_value' => $imovel["SalaoJogos"]],
                    ['post_id' => $post->id, 'meta_key' => 'sauna', 'meta_value' => $imovel["Sauna"]],
                    ['post_id' => $post->id, 'meta_key' => 'closet', 'meta_value' => $imovel["Closet"]],
                    ['post_id' => $post->id, 'meta_key' => 'lareira', 'meta_value' => $imovel["Lareira"]],
                    ['post_id' => $post->id, 'meta_key' => 'entrada_lateral', 'meta_value' => $imovel["EntradaLateral"]],
                    ['post_id' => $post->id, 'meta_key' => 'interfone', 'meta_value' => $imovel["Interfone"]],
                    ['post_id' => $post->id, 'meta_key' => 'mobiliado', 'meta_value' => $imovel["Mobiliado"]],
                    ['post_id' => $post->id, 'meta_key' => 'recuo', 'meta_value' => $imovel["Recuo"]],
                    ['post_id' => $post->id, 'meta_key' => 'promocao', 'meta_value' => $imovel["Promocao"]],
                    ['post_id' => $post->id, 'meta_key' => 'rua', 'meta_value' => $imovel["Endereco"]],
                    ['post_id' => $post->id, 'meta_key' => 'quintal', 'meta_value' => $imovel["Quintal"]],
                    ['post_id' => $post->id, 'meta_key' => 'vestiario', 'meta_value' => $imovel["Vestiario"]],
                    ['post_id' => $post->id, 'meta_key' => 'zona', 'meta_value' => $imovel["Zona"] ? $imovel["Zona"] : ''],
                    ['post_id' => $post->id, 'meta_key' => 'fotos', 'meta_value' => $fotos],
                ];
                // wp_postmeta
                Info::insert($data);
                // dd($data);
            } catch (Exception $e) {
                dd($e);
                //throw $th;
            }
        } else {
            // update

            //removendo todos os postmetas
            $info = Info::query()->where('post_id', $exist->post_id)->delete();

            if (isset($imovel["NumFotos"]) && $imovel["NumFotos"] == 1) {
                $fotos = $imovel["Fotos"]["Foto"]["NomeArquivo"];
            } else {
                $teste = [];
                if (isset($imovel["Fotos"]["Foto"]) && $imovel["Fotos"]["Foto"] != null) {
                    foreach ($imovel["Fotos"]["Foto"] as $item) {
                        $teste[] = $item["NomeArquivo"];
                    }
                    $fotos = implode("|", $teste);
                } else {
                    $fotos = '';
                }
            }

            $servico = null;
            switch ($imovel["Servico"]) {
                case 'L':
                    $servico = 'Alugar';
                    break;
                case 'V':
                    $servico = 'Vender';
                    break;
            }

            try {
                $data = [
                    ['post_id' => $exist->post_id, 'meta_key' => 'status', 'meta_value' => $servico],
                    ['post_id' => $exist->post_id, 'meta_key' => 'referencia', 'meta_value' => $imovel["CodigoImovel"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'cep', 'meta_value' => $imovel["CEP"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'valor_de_venda', 'meta_value' => $imovel["PrecoVenda"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'aluguel', 'meta_value' => $imovel["PrecoLocacao"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'condominio', 'meta_value' => $imovel["PrecoCondominio"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'iptu', 'meta_value' => $imovel["IPTU"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'area_util', 'meta_value' => $imovel["AreaUtil"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'metragem', 'meta_value' => $imovel["AreaTotal"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'content', 'meta_value' => $imovel["Descricao"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'infraestrutura', 'meta_value' => $imovel["Infraestrutura"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'dormitorios', 'meta_value' => $imovel["QtdDormitorios"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'suites', 'meta_value' => $imovel["QtdSuites"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'banheiros', 'meta_value' => $imovel["QtdBanheiros"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'salas', 'meta_value' => $imovel["QtdSalas"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'vagas', 'meta_value' => $imovel["QtdVagas"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'elevadores', 'meta_value' => $imovel["QtdElevador"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'unidades_por_andar', 'meta_value' => $imovel["QtdUnidadesAndar"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'andares', 'meta_value' => $imovel["QtdAndar"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'ano_de_construcao', 'meta_value' => $imovel["AnoConstrucao"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'armario_de_cozinha', 'meta_value' => $imovel["ArmarioCozinha"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'armario_embutido', 'meta_value' => $imovel["ArmarioEmbutido"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'churrasqueira', 'meta_value' => $imovel["Churrasqueira"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'copa', 'meta_value' => $imovel["Copa"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'entrada_de_caminhoes', 'meta_value' => $imovel["EntradaCaminhoes"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'esquina', 'meta_value' => $imovel["Esquina"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'jardim', 'meta_value' => $imovel["Jardim"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'piscina', 'meta_value' => $imovel["Piscina"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'playground', 'meta_value' => $imovel["Playground"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'quadra_poliesportiva', 'meta_value' => $imovel["QuadraPoliEsportiva"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'salao_de_festas', 'meta_value' => $imovel["SalaoFestas"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'salao_de_jogos', 'meta_value' => $imovel["SalaoJogos"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'sauna', 'meta_value' => $imovel["Sauna"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'closet', 'meta_value' => $imovel["Closet"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'lareira', 'meta_value' => $imovel["Lareira"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'entrada_lateral', 'meta_value' => $imovel["EntradaLateral"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'interfone', 'meta_value' => $imovel["Interfone"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'mobiliado', 'meta_value' => $imovel["Mobiliado"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'recuo', 'meta_value' => $imovel["Recuo"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'promocao', 'meta_value' => $imovel["Promocao"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'rua', 'meta_value' => $imovel["Endereco"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'quintal', 'meta_value' => $imovel["Quintal"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'vestiario', 'meta_value' => $imovel["Vestiario"]],
                    ['post_id' => $exist->post_id, 'meta_key' => 'zona', 'meta_value' => $imovel["Zona"] ? $imovel["Zona"] : ''],
                    ['post_id' => $exist->post_id, 'meta_key' => 'fotos', 'meta_value' => $fotos],
                ];
                // wp_postmeta
                Info::insert($data);
            } catch (Exception $e) {
                dd($e);
            }
        }
    };
});
